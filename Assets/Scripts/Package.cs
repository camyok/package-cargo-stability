﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * This class models one package
 */
public class Package : MonoBehaviour
{
    //----------------------------------
    //PUBLIC VARIABLES
    //----------------------------------
	public bool broken;
    //----------------------------------
    //PRIVATE VARIABLES
    //----------------------------------

    //----------------------------------
    //METHODS
    //----------------------------------

  
    /*
     * This method sets the values of a package
     * @param size the Size of the package in meters
     * @param position the position of the package in world space
     * @param packageId the id to be desplayed on the package box
     * @param packageColorP  the color of the package 
     */ 
    public void setPackageValues(Vector3 size,Vector3 position,int packageId,Color packageColor,float weight)
    {
		//JCMF
		//Unbroken initial state
		broken = false;
		//JCMF

//        //Set the position in space
//        transform.parent.position = position;
//        //Set the dimensions of the package
//        transform.parent.localScale = size;
//        //Set the color of the package
//        GetComponentInChildren<Renderer>().material.color = packageColor;
//        //Set the letters of the package
//        TextMesh[] tms = GetComponentsInChildren<TextMesh>();
//        //Set the mass on the rigidbody
//        GetComponentInChildren<Rigidbody>().mass = weight;
//        //Set the txt in the id
//        foreach (TextMesh tm in tms)
//        {
//            tm.text = packageId+"";
//        }

		//Set the position in space
		transform.position = position;
		//Set the dimensions of the package
		transform.localScale = size;
		//Set the color of the package
		GetComponent<Renderer>().material.color = packageColor;
		//Set the mass on the rigidbody
		GetComponentInChildren<Rigidbody>().mass = weight;
	}

    /**
    *Shows the package Information on clicked
    */
    public void showPackageInfo()
    {
        PackageManager.instance.showPackageInfo(int.Parse(GetComponentInChildren<TextMesh>().text));
    }


	//JCMF
	/**
    *Returns the global velocity of the package
    */
	public Vector3 comparePackageVelocity()
	{
		return GetComponentInChildren<Rigidbody>().velocity;
	}

	/**
    *Changes the material of the package to reflect itś state in the DBC
    */
	public void breakPackage()
	{
		broken = true;
	}

	/**
    *Changes the material of the package to reflect itś state in the DBC
    */
	public bool isBroken()
	{
		return broken;
	}
	//JCMF
}
