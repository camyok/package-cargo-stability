﻿using UnityEngine;
using System.Collections;

public class ContainerMovement : MonoBehaviour
{
	//----------------------------------
	//PUBLIC VARIABLES
	//----------------------------------
	//The package manager
	public GameObject manager;
	//Maximum lateral acceleration in G's
	public float lateralAccel=0.8f;
	//Maximum vertical acceleration in G's
	public float verticalAccel=1.0f;
	//Maximum frontal acceleration in G's
	public float forwardAccel=0.5f;
	//Maximum breaking deceleration in G's
	public float brakeAccel=1.2f;
	//The time scale of the simulation, 1.0 being real time and 2.0 being twice as fast 
	public float simulationTimeScale = 1.0f;


	//----------------------------------
	//PRIVATE VARIABLES
	//----------------------------------
	//The simulation time of every frame, used to calculate acceleration by numerical differenciation 
	private float[] moveDeltaTime;
	//The container's rigidbody 
	private Rigidbody rb;
	//The target acceleration of the container 
	private Vector3 targetVel = Vector3.zero;
	//The maximum acceleration of the container during the test 
	private float accel = 0f;
	//Used to adjust de movement of the container
	private float accelOffset = 0f;
	//The velocity of the container 
	public Vector3 moveVel = Vector3.zero;
	//Indicates if a simulation routine isbeing executed 
	private bool isInRoutine = false;
	//The total mass of the continer plus the packages inside 
	private float totalMass=1000f;
	//The total mass of the continer plus the packages inside 
	private float testTime=0f;
	//The state of the simulation 
	private bool testing=false;
	//The test has been completed 
	private bool testDone=false;
	//The current solution being simulated 
	public string currentPath=".\\Assets\\Txt\\1\\1\\Temp_1_1_1.txt";
	//Used to iterate the file path 
	private int iterA=1;
	//Used to iterate the file path 
	private int iterB=1;
	//Used to iterate the file path 
	private int iterC=1;

	// Use this for initialization
	void Start ()
	{
		rb = GetComponent<Rigidbody> ();
		targetVel = Vector3.zero;
		moveDeltaTime = new float[4];
	}
	
	// Update is called once per frame
	void Update ()
	{
		moveDeltaTime[0] = Time.deltaTime;

		if (Input.GetKeyDown ("y")) {
			StartCoroutine("TestOne");
		}

		if (targetVel.sqrMagnitude > 0f) {
			moveDeltaTime[1]=rb.velocity.x;
			moveDeltaTime[2]=rb.velocity.y;
			moveDeltaTime[3]=rb.velocity.z;
			manager.GetComponentInChildren<PackageManager> ().CheckDamage (moveDeltaTime);
		}
	}

	void FixedUpdate ()
	{
		if (testing && !testDone) {
			testTime += Time.deltaTime;
			float offset = accel * 0.25f * Mathf.Cos (2 * testTime)-accelOffset;
			rb.position = offset * targetVel;
		}
//		else if ((transform.position-Vector3.zero).sqrMagnitude>0.001f && !testDone){
//			rb.MovePosition (Vector3.Lerp(transform.position, Vector3.zero, 3*Time.deltaTime));
//		}
	}


	/**
    *Lateral acceleration and deceleration of the container
    */
	IEnumerator TestOne() {
		targetVel = transform.forward;
		Time.timeScale=simulationTimeScale;
		yield return new WaitForSeconds(0.1f);
		manager.GetComponentInChildren<PackageManager> ().GetStartingYPositions();
		testing = true;
		testTime = 0f;
		accel = 9.81f*lateralAccel;
		accelOffset = (accel * 0.25f);
		yield return new WaitForSeconds(3.14f);
		accel = 0f;
		accelOffset = 0f;
		yield return new WaitForSeconds(0.5f);
		testing = false;
		StartCoroutine("TestTwo");
		yield return null;

	}

	/**
    *Forward acceleration and deceleration of the container
    */
	IEnumerator TestTwo() {
		targetVel = transform.right;
		yield return new WaitForSeconds(0.1f);
		testing = true;
		testTime = 0f;
		accel = 9.81f*forwardAccel;
		accelOffset = (accel * 0.25f);
		yield return new WaitForSeconds(1.57f);
		accel = 9.81f*brakeAccel;
		accelOffset = 0f;
		yield return new WaitForSeconds(0.85f);
		manager.GetComponentInChildren<PackageManager> ().GetFinalYPositions ();
		yield return new WaitForSeconds(0.75f);
		testing = false;
		testDone = true;
		yield return null;
	}

	/**
    *Acceleration and deceleration of the container
    */
	public void StartTest()
	{
		isInRoutine = true;
		StartCoroutine("TestOne");
	}

	public void NextTest(){
		currentPath=".\\Assets\\Txt\\1\\1\\Temp_"+iterA+"_"+iterB+"_1.txt";
		manager.GetComponentInChildren<PackageManager> ().AutoTest (currentPath);
		if (iterA < 8) {
			if (iterB == 100) {
				iterA++;
				iterB = 1;
			} else {
				iterB++;
			}
		}
	}
}

