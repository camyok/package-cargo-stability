﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
 * Use this class to connect with MySQL database
 */
public class SQLController : MonoBehaviour
{
    //----------------------------------
    //PUBLIC VARIABLES
    //----------------------------------
    public string[] items;
    //----------------------------------
    //PRIVATE VARIABLES
    //----------------------------------

    //----------------------------------
    //METHODS
    //----------------------------------


    IEnumerator Start()
    {
        WWW query = makeSQLQuery("SELECT * FROM CARGA");
        yield return query;
        string answer = query.text;
        Debug.Log(answer);
        items = answer.Split(';');
        Debug.Log(items.Length);
        foreach (string item in items)
        {
            
            Debug.Log(item);
            Debug.Log(GetDataValue(item, "ID:"));
        }
        
    }

    string GetDataValue(string data, string index)
    {
        string value = "";
        if (data!="")
        {
            value = data.Substring(data.IndexOf(index) + index.Length);
            if (value.Contains("|"))
                value = value.Remove(value.IndexOf("|"));
        }
        
        return value;
    }
  
    /**
      *This method is used to make SQL querys to the SQL Server
      *
      */
    public WWW makeSQLQuery(string query)
    {
        Debug.Log(query);
        query =query.Replace(" ", "%20");
        query = "http://localhost/PackageCargo/PackageCargoData.php?SQL=" + query;        
        WWW itemsData = new WWW(query);
        return itemsData;        
    }
	
	
}
