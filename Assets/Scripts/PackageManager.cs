﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
/*
 * This Class is in charge of managing all packages on a container
 */
public class PackageManager : MonoBehaviour
{
    
    //**************************************************************************
    //The definiton of a package type
    public class PackageType
    {
              
        //----------------------------------
        //PUBLIC VARIABLES
        //----------------------------------
        //The Size of the package
        //x = width
        // y = height
        //z = lenght
        public Vector3 packageSize;
        //The Color of the Package
        public Color packageColor;
        //The id of the package
        public int packageId;
        //the number of packages
        public int quantity;
        //weight of the package
        public float weight;
        //The ui package controller
        public UIPackage uiPackage;
        //Positions for each package of this group
        public List<Vector3> packagePositions;
        //End position for each package
        public List<Vector3> endPositions;
        //----------------------------
        //Methods
        //----------------------------
        public PackageType(Color packageColorP, int packageIdP, Vector3 packageSizeP, int quantityP, float weightP, UIPackage uiPackageP)
        {
            packageColor = packageColorP;
            packageSize = packageSizeP;
            packageId = packageIdP;
            quantity = quantityP;
            weight = weightP;
            uiPackage = uiPackageP;
            packagePositions = new List<Vector3>();
            endPositions = new List<Vector3>();
        }
        public void updatePType(Color packageColorP, int packageIdP, Vector3 packageSizeP, int quantityP, float weightP, UIPackage uiPackageP)
        {
            packageColor = packageColorP;
            packageSize = packageSizeP;
            packageId = packageIdP;
            quantity = quantityP;
            weight = weightP;
            uiPackage = uiPackageP;           
        }
        public void AddPosition(Vector3 initialPosition,Vector3 endPosition)
        {
            packagePositions.Add(initialPosition);
            endPositions.Add(endPosition);
        }
       
    }
    //**************************************************************************
    //----------------------------------
    //PUBLIC VARIABLES
    //----------------------------------
    //The file path to the txt
    //example .\Assets\Chep\chep.txt
    public string filePath;
    //The prefab of the gameobject to be placed on the UI
    public GameObject UIPrefab;
    //The list where the element is going to be placed
    public RectTransform UIList;   
    //Singleton instance
    public static PackageManager instance;
    //The prefab of a package
    public GameObject packagePrefab;
    
    //The Color of the Package
    public Color[] packageColors;
    //Error message UI
    public GameObject errorMessage;
    //Info Panel
    public UIPackage infoPanel;
    //Size of the container in m
    public Vector3 containerSize;
    //The transform of the container
    public Transform containerTransform;
    //The Game object of the UI for a new container size


	//JCMF
	//The Rigidbody of the container
	public GameObject containerObject;
	//JCMF

    public GameObject UIContainerSizeGO;
    //The list where the containerSize is going to be placed
    public RectTransform UIListVehicles;
    //----------------------------------
    //PRIVATE VARIABLES
    //----------------------------------
    //File, reader and text for reading purposes
    private FileInfo theSourceFile = null;
    private StreamReader reader = null;
    private string text = " "; // assigned to allow first line to be read below
    //last id used
    private int lastId;
    //The package types for each package
    private Dictionary<int,PackageType> pTypes;
    //The packages to be managed
    private List<GameObject> packages;


	//----------------------------------
	//STABILITY VARIABLES
	//----------------------------------
	//The mass of the packages
	private float packageMass;
	//The starting delta for the packages, used to obtain M3 
	private float[] yDelta;
	//How far, in meters, must the packages change position to be considered as fallen
	public float fallThreshold = 0.05f;
	//The metric indicating the number of fallen boxes
	private float m3;
	//How fast, in meters per second, can the packages move in respect to the container
	public float velocityDelta = 5f;
	//The acceleration, in meters per second squared, that will cause the package to break
	public float allowableAcceleration = 5f;
	//The metric indicating the number of boxes within the damage boundary curve
	private float m4;
    //----------------------------------
    //METHODS
    //----------------------------------
    //Singleton
    void Awake()
    {       
        //Check if instance already exists
        if (instance == null)
            //if not, set instance to this
            instance = this;
        //If instance already exists and it's not this:
        else if (instance != this)
            //Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
            Destroy(gameObject);

        //Sets this to not be destroyed when reloading scene
        //DontDestroyOnLoad(gameObject);
    }
    // Use this for initialization
    void Start ()
    {
        infoPanel.gameObject.SetActive(false);
        errorMessage.SetActive(false);
        lastId = 0;
        pTypes = new Dictionary<int,PackageType>();
        packages = new List<GameObject>();
        resizeContainer(containerSize);     
    }
    /**
     * Method that creates a new package type
     * @param packageColorP The color of the package type
     * @param packageIdP    The id of the package type
     * @param packageSizeP The size of the package type width, height, lenght in meters
     * @param quantityP The number of packages of these package type
     * @param weightP The weight of the package type in Kg
     * @param uiPackageP the UI controller of the package
     *@param determines if isEmpty
     */
    public void createNewPackageType(Vector3 packageSizeP,int quantityP, float weightP,bool isEmpty,int idP)
    {
        Color packageColorP = packageColors[0];
        if (lastId < packageColors.Length)
            packageColorP = packageColors[lastId];
       
           
        //        char packageIdP = ALPHABET[lastId+1];
        Debug.Log("Creating package " + idP);
        lastId++;
        GameObject go = GameObject.Instantiate(UIPrefab);
        go.transform.SetParent(UIList);
        UIPackage uiPackageP = go.GetComponent<UIPackage>();
        //Create a new package type then add it to the list
        PackageType p = new PackageType(packageColorP, idP, packageSizeP, quantityP, weightP,uiPackageP);
        uiPackageP.updateValues(packageColorP, idP, packageSizeP, quantityP, weightP, isEmpty);
        pTypes.Add(idP,p);
        
    }
    /**
    * Method that updates a package type
    * @param packageColorP The color of the package type
    * @param packageIdP    The id of the package type
    * @param packageSizeP The size of the package type width, height, lenght in meters
    * @param quantityP The number of packages of these package type
    * @param weightP The weight of the package type in Kg
    * @param uiPackageP the UI controller of the package
    */
    public void updatePackageValues(Color packageColorP, int packageIdP, Vector3 packageSizeP, int quantityP, float weightP)
    {      
          
        try
        {
            Debug.Log("Updating package " + packageIdP + " # " + quantityP);
            UIPackage uiPackageP = pTypes[packageIdP].uiPackage;
            pTypes[packageIdP].updatePType(packageColorP, packageIdP, packageSizeP, quantityP, weightP, uiPackageP);
            uiPackageP.updateValues(packageColorP, packageIdP, packageSizeP, quantityP, weightP, false);
            infoPanel.updateValues(packageColorP, packageIdP, packageSizeP, quantityP, weightP, false);
            Debug.Log("Updated package " + packageIdP + " # " + quantityP);
            return;            
        }
        catch (Exception )
        {
           Debug.Log("Error on update package ");
        }          
        
    }
    /**
     * This method adds a UI Element and creates an empty package type
     */
    public void createUIPackage()
    {
        int idP = 0;
        while (pTypes.ContainsKey(idP))
        {
            idP++;
        }
        createNewPackageType(new Vector3(), 0, 0, true,idP);        
    }
    //Load the packages in the space
    public void loadPackages()
    {
        foreach (GameObject pack in packages)
        {
            GameObject.Destroy(pack);
        }
        packages.Clear();
        
        //Load the packages
        
        foreach (PackageType pType in pTypes.Values)
        {
            for (int i = 0; i < pType.quantity; i++)
            {
                GameObject go = GameObject.Instantiate(packagePrefab);
                packages.Add(go);
        
                go.GetComponent<Package>().setPackageValues(pType.endPositions[i]-pType.packagePositions[i], pType.packagePositions[i], pType.packageId, pType.packageColor,pType.weight);
               
            }
        }
        Debug.Log("Loaded "+packages.Count+" packages");
    }

    /**
      *Deletes a package type with and ID
      *@id the id of the package
      */
      public void deletePackageType(int id)
    {
        PackageType item;
        pTypes.TryGetValue(id, out item);
        if(pTypes.ContainsKey(id))
        {
            Debug.Log("Deleted " + id);
            GameObject.Destroy( item.uiPackage.gameObject);
            infoPanel.gameObject.SetActive(false);
            pTypes.Remove(id);
            return;    
        }        
    }

    /**
    *Enables or disables error UI
    */
    public void switchErrorUI(bool uistate)
    {
        errorMessage.SetActive(uistate);
    }

    /**
      *Shows package info on UI
      *@param id of the package
      */
      public void showPackageInfo(int id)
    {
        PackageType item;
        pTypes.TryGetValue(id, out item);
        if (pTypes.ContainsKey(id))
        {
            infoPanel.gameObject.SetActive(true);
            infoPanel.updateValues(item.packageColor, item.packageId, item.packageSize, item.quantity, item.weight, false);
            return;
        }
        
    }

    /**
      *Resizes the container
      @param newSize the new Size of the container
      */
     public void resizeContainer(Vector3 newSize)
    {
        containerSize = newSize;
        containerTransform.localScale = containerSize;
        containerTransform.position = Vector3.zero;
    }

    /**
      *Adds a new container size on the UI
      */
      public void addNewContainerSizeUI(Vector3 newSize)
    {
        GameObject go = GameObject.Instantiate(UIContainerSizeGO);
        go.transform.SetParent(UIListVehicles);
        UIContainer uiContainerP = go.GetComponent<UIContainer>();
        uiContainerP.setCustomValues(newSize,"Loaded Container");
    }
    //--------------------------------------------------------------
    //Package load from txt file
    //--------------------------------------------------------------
    /**
     *Reads the txt file and loads the info
     */
    public void ReadTxt()
    {

        //Initialize the reading
        theSourceFile = new FileInfo(filePath);
        reader = theSourceFile.OpenText();

        //First Line
        text = reader.ReadLine();
        string[] split = text.Split('\t');
        int totalPcs = int.Parse(split[0]);
        resizeContainer(new Vector3(float.Parse(split[2]), float.Parse(split[4]), float.Parse(split[3])) / 100);
        addNewContainerSizeUI(new Vector3(float.Parse(split[2]), float.Parse(split[4]), float.Parse(split[3])) / 100);
        Debug.Log("Total pcs = " + split[0] + " # Destinations = " + split[1]);
        Debug.Log("Container size = " + split[2] + "mm " + split[4] + "mm " + split[3] + "mm");
        //Second Line
        text = reader.ReadLine();
        split = text.Split('\t');
        Debug.Log("Total Volume = " + split[0] + " Used Volume = " + split[1]);
        //Read the packages

        while (text != null)
        {
            text = reader.ReadLine();
            split = text.Split('\t');
            //Is it over?
            if (split[8] == "0")
                return;
            //Read Package
            Debug.Log(text);
            Vector3 endPosition = new Vector3(float.Parse(split[3]), float.Parse(split[5]), float.Parse(split[4])) / 100;
            Vector3 packPosition = new Vector3(float.Parse(split[0]), float.Parse(split[2]), float.Parse(split[1])) / 100;
            Vector3 packSize = endPosition - packPosition;
            int packId = int.Parse(split[6]);
            int packGroup = int.Parse(split[7]);
            int packContainerId = int.Parse(split[8]);
             
            //Check If package type exist 
            if(!pTypes.ContainsKey(packId))
                createNewPackageType(packSize, 0, 1, false, packId);
           
            PackageType pType = pTypes[packId];
            pType.AddPosition(packPosition,endPosition);
            updatePackageValues(pType.packageColor, packId, packSize, pType.quantity + 1, pType.weight);
            
        }
    }

	//--------------------------------------------------------------
	//Dynamic Stability Metrics
	//--------------------------------------------------------------
	/**
     *Determines the starting "Y" position of the packages relative to the container 
     *To be called after the packages have been instantiated
     */
	public void GetStartingYPositions()
	{
		m3 = 0;
		yDelta = new float[packages.Count]; 
		for(int f = 0; f < packages.Count; f++)
		{
			yDelta [f] = packages [f].GetComponentInChildren<Rigidbody> ().position.y-containerTransform.position.y;
		}
	}

	/**
     *Compares the starting and final "Y" positions of the packages relative to the container 
     *To be called after the dynamic simulation is finished
     */
	public void GetFinalYPositions()
	{
		int fallen = 0;
		float yDeltaFinal = 0;
		for(int f = 0; f < packages.Count; f++)
		{
			yDeltaFinal = packages [f].GetComponentInChildren<Rigidbody> ().position.y-containerTransform.position.y;
			if (Math.Abs (yDelta [f] - yDeltaFinal) > fallThreshold) {
				fallen++;
			}
		}
		m3 = fallen;
		print ("m3: "+m3);
		print ("m4: "+m4);
		WriteMetrics (";" + m3 + ";" + m4+"\n");
	}

	/**
     *Checks the position of every package in respect to the DBC,
     *To be called during Update, FixedUpdate, or LateUpdate
     */
	public void CheckDamage(float[] moveDeltaTime)
	{
		Vector3 relativeVelocity = Vector3.zero;
		Vector3 containerVelocity = new Vector3 (moveDeltaTime [1], moveDeltaTime [2], moveDeltaTime [3]);
		Vector3 acceleration = Vector3.zero;
		Vector3[] lastVelocity = new Vector3[packages.Count+1];
		lastVelocity [0] = Vector3.zero;
		for(int f = 0; f < packages.Count; f++)
		{
			if (!packages [f].GetComponentInChildren<Package> ().isBroken()) {
				relativeVelocity = packages [f].GetComponentInChildren<Rigidbody> ().velocity-containerVelocity;
				if (relativeVelocity.sqrMagnitude > velocityDelta*velocityDelta) {
					packages [f].GetComponentInChildren<Package> ().breakPackage();
					m4++;
				}
				else if (((relativeVelocity-lastVelocity [f]).sqrMagnitude)/Time.deltaTime > allowableAcceleration*allowableAcceleration) {
					packages [f].GetComponentInChildren<Package> ().breakPackage();
					m4++;
				}
				lastVelocity [f+1] = relativeVelocity;
			}
		}
		//		print (m4);
	}

	/**
     *Returns the total mass of the packages
     */
	public float GetMass(){
		return packageMass;
	}

	//--------------------------------------------------------------
	//Automatic testing
	//--------------------------------------------------------------
	/**
     *Runs the dynamic simulation and obtains the metrics for the particular case.
     */
	public void AutoTest(String tempPath)
	{
		filePath = tempPath;
		ReadTxt ();
		loadPackages ();
		containerObject.SendMessage ("StartTest");
	}

	public void WriteMetrics(String metrics){
		if (File.Exists (".\\Assets\\Txt\\1\\1\\results1.txt")) {
			System.IO.File.AppendAllText (".\\Assets\\Txt\\1\\1\\results1.txt", metrics);
		} 
		else {
			File.CreateText(".\\Assets\\Txt\\1\\1\\results1.txt");
		}
	}
}
